//
//  testeTests.swift
//  testeTests
//
//  Created by Anderson Lucas C. Ramos on 16/12/16.
//  Copyright © 2016 Globosat. All rights reserved.
//

import XCTest
@testable import teste

class testeTests: XCTestCase, SessionDelegate, OmdbSearcherDelegate, MovieDataDelegate {
	fileprivate var session: Session!
	fileprivate var favorites: FavoritesManager!
	fileprivate var parser: MovieListParser!
	fileprivate var searcher: OmdbSearcher!
	fileprivate var movieLoader: MovieObjectDataLoader!
	
	fileprivate var expectation: XCTestExpectation!
	
	fileprivate var searchResults: [MovieObject]!
	
    override func setUp() {
        super.setUp()
		
		self.session = Session()
		self.session.delegate = self
		self.favorites = FavoritesManager()
		self.parser = MovieListParser()
		self.searcher = OmdbSearcher()
		self.searcher.delegate = self
		
		self.movieLoader = MovieObjectDataLoader()
		self.movieLoader.delegate = self
		
		self.searchResults = nil
    }
    
    override func tearDown() {
		self.session = nil
		self.favorites = nil
		self.parser = nil
		self.searcher = nil
		self.movieLoader = nil
		
		self.expectation = nil
		
		self.searchResults = nil
		
        super.tearDown()
    }
    
    func testParserPerformance() {
        self.measure { [weak self] in
			_ = self?.parser.loadMovieList()
        }
    }
	
	func testFavorites() {
		let movieList: [MovieObject]! = self.parser.loadMovieList()
		
		assert(movieList.count > 0)
		
		let index: Int = Int(arc4random_uniform(UInt32(movieList.count - 1)))
		let randomObject: MovieObject = movieList[index]
		
		assert(!self.favorites.isMovieFavorited(randomObject))
		self.favorites.add(randomObject)
		assert(self.favorites.isMovieFavorited(randomObject))
		self.favorites.remove(randomObject)
		assert(!self.favorites.isMovieFavorited(randomObject))
	}
	
	func testLogin() {
		self.expectation = self.expectation(description: "login expectation")
		
		self.session.login(withEmail: "test@globosat.com", andPassword: "123456")
		
		self.waitForExpectations(timeout: 10.0, handler: nil)
		
		assert(self.session.isAuthenticated)
	}
	
	func testSearch() {
		self.expectation = self.expectation(description: "search expectation")
		
		self.searcher.searchOmdb(byMovieTitle: "Star Wars")
		
		self.waitForExpectations(timeout: 10.0, handler: nil)
		
		assert(self.searchResults.count > 0)
		
		self.favorites.add(self.searchResults[0])
		assert(self.favorites.isMovieFavorited(self.searchResults[0]))
	}
	
	func testMovieObjectDataLoad() {
		self.expectation = self.expectation(description: "movie object data load expectation")
		
		let movieList: [MovieObject]! = self.parser.loadMovieList()
		self.movieLoader.loadDetailedData(forObject: movieList[0])
		
		self.waitForExpectations(timeout: 10.0, handler: nil)
		
		assert(movieList[0].title != nil)
		assert(movieList[0].thumbImage != nil)
	}
	
	////// Session Delegate Impl
	
	func session(didAuthenticateWithSuccess success: Bool) {
		self.expectation.fulfill()
	}
	
	func session(connectionError err: Error?) {
		self.expectation.fulfill()
	}
	
	func sessionCannotProceedWithEmail() {
		
	}
	
	func sessionCannotProceedWithPassword() {
		
	}
	
	////// Omdb Searcher Impl
	
	func omdbSearcher(didFindMovies movies: [MovieObject]) {
		self.searchResults = movies
		
		self.expectation.fulfill()
	}
	
	func omdbSearcherDidFailedFindingMovies() {
		self.expectation.fulfill()
	}
	
	///// Movie Data Delegate Impl
	
	func movieDidLoadData(_ movie: MovieObject) {
		self.expectation.fulfill()
	}
	
	func movieDidFailedToLoadData(_ movie: MovieObject) {
		self.expectation.fulfill()
	}
}
