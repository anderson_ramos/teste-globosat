README

Nome: Anderson Lucas de Castro Ramos

Implementado app de testes da seleção da Globosat.

Obs:

- Implementei utilizando Swift 3, no teste pedia o swift 2, mas eu tenho conhecimento de Swift 1, 2 e 3. Espero que não seja um problema.
- Utilizei o serviço de pesquisa de filmes do Imdb pela API gratuita do site http://www.omdbapi.com para que o app utilizasse dados reais.
- O app foi feito suportando Force Touch, ou não em devices que não tem a funcionalidade.
- Encontrei um bug agora antes de enviar. Mas para não perder o prazo, fica pra segunda versão :).
	- Se você entrar na tela de favoritos e for em pesquisar. Dentro da pesquisa, se você remover algum filme dos favoritos ele não 
	  sai da lista de pesquisa nem da lista de favoritos na tela anterior, apenas enquanto vc não sair e voltar da tela. 
	  Se remover um filme direto na tela de favoritos, funciona normalmente.