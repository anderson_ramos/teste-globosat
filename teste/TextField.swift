//
//  TextView.swift
//  teste
//
//  Created by Anderson Lucas C. Ramos on 18/12/16.
//  Copyright © 2016 Globosat. All rights reserved.
//

import UIKit

class TextField: UITextField {
	override func textRect(forBounds bounds: CGRect) -> CGRect {
		return bounds.insetBy(dx: 10, dy: 0)
	}
	
	override func editingRect(forBounds bounds: CGRect) -> CGRect {
		return bounds.insetBy(dx: 10, dy: 0)
	}
}

class Label: UILabel {
	override func textRect(forBounds bounds: CGRect, limitedToNumberOfLines numberOfLines: Int) -> CGRect {
		return bounds.insetBy(dx: 20, dy: 0)
	}
}
