//
//  MovieViewerViewController.swift
//  teste
//
//  Created by Anderson Lucas C. Ramos on 17/12/16.
//  Copyright © 2016 Globosat. All rights reserved.
//

import UIKit

class MovieViewerViewController: UIViewController, MovieDataDelegate {
	@IBOutlet fileprivate weak var posterImageView: UIImageView!
	@IBOutlet fileprivate weak var movieTitleLabel: UILabel!
	@IBOutlet fileprivate weak var movieRatingLabel: UILabel!
	@IBOutlet fileprivate weak var movieDurationLabel: UILabel!
	@IBOutlet fileprivate weak var movieAwardsLabel: UILabel!
	@IBOutlet fileprivate weak var movieReleaseLabel: UILabel!
	@IBOutlet fileprivate weak var movieGenreLabel: UILabel!
	@IBOutlet fileprivate weak var movieDirectorLabel: UILabel!
	@IBOutlet fileprivate weak var moviePlotTextView: UITextView!
	
	@IBOutlet fileprivate weak var backButton: UIButton!
	@IBOutlet fileprivate weak var favoriteButton: UIButton!
	
	@IBOutlet fileprivate weak var moviePlotTextViewHeightConstraint: NSLayoutConstraint!
	
	fileprivate(set) var favoritesManager: FavoritesManager = FavoritesManager()
	
	var movieObject: MovieObject!
	weak var movieLoader: MovieObjectDataLoader?
	
	deinit {
		self.movieObject = nil
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()

		self.movieLoader?.delegate = self
		
		self.setupContent()
	}
	
	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
		
		self.calculateAndSetPlotHeight()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		self.navigationController?.setNavigationBarHidden(true, animated: true)
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		
		self.navigationController?.setNavigationBarHidden(false, animated: true)
	}
	
	@available(iOS 9.0, *)
	override var previewActionItems: [UIPreviewActionItem] {
		// TODO: check if its in the favorites and change the action to remove
		var actions: [UIPreviewActionItem] = [UIPreviewActionItem]()
		if self.favoritesManager.isMovieFavorited(self.movieObject) {
			actions.append(UIPreviewAction(title: "Remover dos favoritos", style: .default, handler: { [weak self] (action: UIPreviewAction, controller: UIViewController) in
				guard let manager = self?.favoritesManager else { return }
				manager.remove(self!.movieObject)
			}))
		} else {
			actions.append(UIPreviewAction(title: "Adicionar aos favoritos", style: .default, handler: { [weak self] (action: UIPreviewAction, controller: UIViewController) in
				guard let manager = self?.favoritesManager else { return }
				manager.add(self!.movieObject)
			}))
		}
		return actions
	}
	
	fileprivate func setupContent() {
		self.posterImageView.image = self.movieObject.thumbImage
		self.movieTitleLabel.text = self.movieObject.title
		self.movieRatingLabel.text = self.getAttributedText(prefix: "Classificação", suffix: self.movieObject.rating)
		self.movieDurationLabel.text = self.getAttributedText(prefix: "Duração", suffix: self.movieObject.duration)
		self.movieAwardsLabel.text = self.getAttributedText(prefix: "Premiações", suffix: self.movieObject.awards)
		self.movieReleaseLabel.text = self.getAttributedText(prefix: "Lançamento", suffix: self.movieObject.releaseDate)
		self.movieGenreLabel.text = self.getAttributedText(prefix: "Gênero", suffix: self.movieObject.genre)
		self.movieDirectorLabel.text = self.getAttributedText(prefix: "Diretor", suffix: self.movieObject.director)
		self.moviePlotTextView.text = self.movieObject.plot
		self.updateFavoritesButtonTitle()
	}
	
	fileprivate func getAttributedText(prefix: String, suffix: String?) -> String {
		let text: String = suffix != nil ? suffix! : "n/a"
		return "\(prefix): \(text)"
	}
	
	fileprivate func calculateAndSetPlotHeight() {
		self.moviePlotTextView.frame.size = self.moviePlotTextView.sizeThatFits(CGSize(width: self.moviePlotTextView.frame.width, height: CGFloat.greatestFiniteMagnitude))
		self.moviePlotTextViewHeightConstraint.constant = self.moviePlotTextView.frame.height
	}
	
	fileprivate func updateFavoritesButtonTitle() {
		if self.favoritesManager.isMovieFavorited(self.movieObject) {
			self.favoriteButton.setTitle("Remover favorito", for: .normal)
		} else {
			self.favoriteButton.setTitle("Adicionar favorito", for: .normal)
		}
	}
	
	@IBAction fileprivate func backButton(sender: UIButton?) {
		_ = self.navigationController?.popViewController(animated: true)
	}
	
	@IBAction fileprivate func favoriteButton(sender: UIButton?) {
		if self.favoritesManager.isMovieFavorited(self.movieObject) {
			self.favoritesManager.remove(self.movieObject)
			self.updateFavoritesButtonTitle()
		} else {
			self.favoritesManager.add(self.movieObject)
			self.updateFavoritesButtonTitle()
		}
	}
	
	///// Movie Data Delegate Impl
 
	func movieDidLoadData(_ movie: MovieObject) {
		self.setupContent()
	}
	
	func movieDidFailedToLoadData(_ movie: MovieObject) {
		let alert: UIAlertController = UIAlertController(title: "Falha", message: "Não foi possível carregar os dados do filme.", preferredStyle: .alert)
		alert.addAction(UIAlertAction(title: "Fechar", style: .cancel, handler: { (_: UIAlertAction) in
			
		}))
		self.present(alert, animated: true, completion: nil)
	}
}
