//
//  MovieListParser.swift
//  teste
//
//  Created by Anderson Lucas C. Ramos on 16/12/16.
//  Copyright © 2016 Globosat. All rights reserved.
//

import Foundation

class MovieListParser {
	init() {
	
	}
	
	func loadMovieList() -> [MovieObject] {
		let movieDict: NSDictionary? = self.parseLocalMovies()
		if self.isValidMovieDict(movieDict) {
			let movieList: [NSDictionary] = movieDict!["movieList"] as! [NSDictionary]
			
			var list: [MovieObject] = [MovieObject]()
			for mv in movieList {
				list.append(MovieObject(info: mv))
			}
			return list
		}
		
		return [MovieObject]()
	}
	
	func loadFavoritesList() -> [MovieObject] {
		var movieList: [MovieObject] = [MovieObject]()
		let idList: [String] = FavoritesManager().movieList
		for id in idList {
			movieList.append(MovieObject(info: ["imdbID": id]))
		}
		return movieList
	}
	
	fileprivate func isValidMovieDict(_ dict: NSDictionary?) -> Bool {
		return dict != nil && dict!["success"] as! Bool
	}
	
	fileprivate func parseLocalMovies() -> NSDictionary? {
		let filePath: String = Bundle.main.path(forResource: "movies", ofType: "json")!
		let fileContent: Data? = try? Data(contentsOf: URL(fileURLWithPath: filePath))
		if fileContent != nil {
			return self.dictionaryFromJson(fileContent!)
		}
		print("MovieListParser - could not load movies.json file!")
		return nil
	}
	
	fileprivate func dictionaryFromJson(_ data: Data) -> NSDictionary? {
		return try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as! NSDictionary
	}
}
