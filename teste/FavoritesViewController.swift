//
//  FavoritesViewController.swift
//  teste
//
//  Created by Anderson Lucas C. Ramos on 17/12/16.
//  Copyright © 2016 Globosat. All rights reserved.
//

import UIKit

class FavoritesViewController: MoviesViewController, FavoritesManagerDelegate {
	@IBOutlet fileprivate weak var noMoviesLabel: UILabel!
	
	fileprivate var mvList: [MovieObject]!
	
	deinit {
		self.mvList = nil
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.favoritesManager.delegate = self
		
		self.showNoMoviesLabelIfNeeded()
	}
	
	override func loadMovieList() -> [MovieObject] {
		if self.mvList == nil {
			self.mvList = MovieListParser().loadFavoritesList()
		}
		return self.mvList
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		self.collectionView?.reloadData()
	}
	
	override func detailViewController(forItem index: Int) -> MovieViewerViewController {
		let controller = super.detailViewController(forItem: index)
		controller.favoritesManager.delegate = self
		return controller
	}
	
	override func showSearchViewController() {
		let controller = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "searchMovies") as! SearchMoviesViewController
		controller.movieList = self.movieList
		self.navigationController?.pushViewController(controller, animated: false)
	}
	
	fileprivate func reloadCollectionViewDelaied() {
		DispatchQueue.main.asyncAfter(deadline: .now() + 0.25) { [weak self] in
			self?.reloadMovieList()
			self?.showNoMoviesLabelIfNeeded()
			self?.collectionView?.reloadData()
		}
	}
	
	fileprivate func showNoMoviesLabelIfNeeded() {
		self.noMoviesLabel.isHidden = self.mvList.count > 0
	}
	
	func favorites(didAddFavorite favorite: MovieObject) {
		self.mvList.append(favorite)
		
		self.reloadCollectionViewDelaied()
	}
	
	func favorites(didRemoveFavorite favorite: MovieObject) {
		self.mvList.remove(object: favorite)
		
		self.reloadCollectionViewDelaied()
	}
}
