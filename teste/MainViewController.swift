//
//  ViewController.swift
//  teste
//
//  Created by Anderson Lucas C. Ramos on 16/12/16.
//  Copyright © 2016 Globosat. All rights reserved.
//

import UIKit

class MainViewController: UIViewController, SessionDelegate {
	@IBOutlet fileprivate weak var loginView: UIView!
	@IBOutlet fileprivate weak var emailTextField: UITextField!
	@IBOutlet fileprivate weak var passwordTextField: UITextField!
	@IBOutlet fileprivate weak var loginButton: UIButton!
	
	fileprivate let session: Session = Session()

	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.session.delegate = self
		
		if self.session.isAuthenticated {
			self.loginView.removeFromSuperview()
		}
	}
	
	@IBAction fileprivate func loginButton(sender: UIButton?) {
		self.session.login(withEmail: self.emailTextField.text!, andPassword: self.passwordTextField.text!)
	}
	
	fileprivate func animateLoginViewAndRemove() {
		UIView.animate(withDuration: 0.25, animations: {
			self.loginView.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
			self.loginView.alpha = 0.0
		}) { [weak self] (finished: Bool) in
			if finished {
				self?.loginView.removeFromSuperview()
			}
		}
	}
	
	fileprivate func showAlert(title: String, message: String) {
		let alert: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
		alert.addAction(UIAlertAction(title: "Fechar", style: .cancel, handler: nil))
		self.present(alert, animated: true, completion: nil)
	}
	
	/// Session Delegate Impl
	
	func session(didAuthenticateWithSuccess success: Bool) {
		if success {
			self.animateLoginViewAndRemove()
		} else {
			self.showAlert(title: "Login", message: "Não foi possível efetuar o login, verifique o email e/ou senha.")
		}
	}
	
	func session(connectionError err: Error?) {
		self.showAlert(title: "Login", message: "Não foi possível estabelecer conexão com a internet. Verifique sua conexão e tente novamente.")
	}
	
	func sessionCannotProceedWithEmail() {
		self.showAlert(title: "Login", message: "Email inválido, verifique seu email.")
	}
	
	func sessionCannotProceedWithPassword() {
		self.showAlert(title: "Login", message: "A senha deve conter no mínimo 6 caracteres.")
	}
}

