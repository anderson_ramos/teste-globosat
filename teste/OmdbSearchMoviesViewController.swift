//
//  OmdbSearchMovieViewController.swift
//  teste
//
//  Created by Anderson Lucas C. Ramos on 17/12/16.
//  Copyright © 2016 Globosat. All rights reserved.
//

import UIKit

class OmdbSearchMoviesViewController: SearchMoviesViewController, OmdbSearcherDelegate {
	fileprivate var omdbSearcher: OmdbSearcher = OmdbSearcher()
	fileprivate var mvList: [MovieObject] = [MovieObject]()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.omdbSearcher.delegate = self
	}
	
	override func loadMovieList() -> [MovieObject] {
		return self.mvList
	}
	
	override func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
		self.omdbSearcher.searchOmdb(byMovieTitle: searchText)
	}
	
	func omdbSearcher(didFindMovies movies: [MovieObject]) {
		self.mvList = movies
		self.reloadMovieList()
		self.collectionView?.reloadData()
	}
	
	func omdbSearcherDidFailedFindingMovies() {
		let alert: UIAlertController = UIAlertController(title: "Falha", message: "Não foi possível buscar por filmes.", preferredStyle: .alert)
		alert.addAction(UIAlertAction(title: "Fechar", style: .cancel, handler: { (_: UIAlertAction) in
			
		}))
		self.present(alert, animated: true, completion: nil)
	}
}
