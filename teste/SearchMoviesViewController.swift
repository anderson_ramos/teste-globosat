//
//  SearchMoviesViewController.swift
//  teste
//
//  Created by Anderson Lucas C. Ramos on 17/12/16.
//  Copyright © 2016 Globosat. All rights reserved.
//

import UIKit

class SearchMoviesViewController : MoviesViewController, UISearchBarDelegate {
	@IBOutlet fileprivate weak var searchController: UISearchController!
	fileprivate weak var searchBar: UISearchBar!
	fileprivate var movieFilteredList: [MovieObject] = [MovieObject]()
	
	override func viewDidLoad() {
		super.viewDidLoad()
	}
	
	override func loadMovieList() -> [MovieObject] {
		return self.movieList
	}
	
	override func setupActionButton() {
		self.navigationItem.setHidesBackButton(true, animated: false)
		self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel,
		                                                         target: self,
		                                                         action: #selector(SearchMoviesViewController.cancelButton(sender:)))
		let searchBar = UISearchBar()
		searchBar.placeholder = "Procurar Filmes"
		searchBar.showsCancelButton = false
		searchBar.delegate = self
		self.navigationItem.titleView = searchBar
		searchBar.becomeFirstResponder()
		self.searchBar = searchBar
	}
	
	@objc fileprivate func cancelButton(sender: UIBarButtonItem?) {
		_ = self.navigationController?.popViewController(animated: false)
	}
	
	override func movieListCount() -> Int {
		if !self.searchBar.text!.isEmpty && self.movieFilteredList.count > 0 {
			return self.movieFilteredList.count
		}
		return super.movieListCount()
	}
	
	override func movieListItem(_ index: Int) -> MovieObject {
		if !self.searchBar.text!.isEmpty && self.movieFilteredList.count > 0 {
			return self.movieFilteredList[index]
		}
		return super.movieListItem(index)
	}
	
	func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
		self.movieFilteredList.removeAll()
		
		for object in self.movieList {
			if object.title != nil && object.title!.uppercased().contains(searchText.uppercased()) {
				self.movieFilteredList.append(object)
			}
		}
		
		self.collectionView?.reloadData()
	}
}
