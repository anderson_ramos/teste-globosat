//
//  FavoritesManager.swift
//  teste
//
//  Created by Anderson Lucas C. Ramos on 17/12/16.
//  Copyright © 2016 Globosat. All rights reserved.
//

import UIKit

protocol FavoritesManagerDelegate : class {
	func favorites(didAddFavorite favorite: MovieObject)
	func favorites(didRemoveFavorite favorite: MovieObject)
}

class FavoritesManager {
	var movieList: [String] {
		return self.getFavoritesArray(mutable: false) as! [String]
	}
	
	weak var delegate: FavoritesManagerDelegate?
	
	init() {
		if UserDefaults.standard.object(forKey: "favorites") == nil {
			UserDefaults.standard.set([String](), forKey: "favorites")
			UserDefaults.standard.synchronize()
		}
	}

	func add(_ object: MovieObject) {
		let favoritesArray = self.getFavoritesArray(mutable: true) as! NSMutableArray
		
		favoritesArray.add(object.id)
		
		self.saveFavoritesArray(favoritesArray)
		
		self.delegate?.favorites(didAddFavorite: object)
	}
	
	func isMovieFavorited(_ object: MovieObject) -> Bool {
		let favoritesArray = self.getFavoritesArray(mutable: false)
		
		return favoritesArray.contains(object.id)
	}
	
	func remove(_ object: MovieObject) {
		let favoritesArray = self.getFavoritesArray(mutable: true) as! NSMutableArray
		
		favoritesArray.remove(object.id)
		
		self.saveFavoritesArray(favoritesArray)
		
		self.delegate?.favorites(didRemoveFavorite: object)
	}
	
	fileprivate func getFavoritesArray(mutable: Bool) -> NSArray {
		let stringArray = UserDefaults.standard.stringArray(forKey: "favorites")!
		if mutable {
			return NSMutableArray(array: stringArray)
		}
		
		return NSArray(array: stringArray)
	}
	
	fileprivate func saveFavoritesArray(_ array: NSMutableArray) {
		UserDefaults.standard.set(array, forKey: "favorites")
		UserDefaults.standard.synchronize()
	}
}
