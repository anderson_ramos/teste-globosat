//
//  OmdbSearcher.swift
//  teste
//
//  Created by Anderson Lucas C. Ramos on 17/12/16.
//  Copyright © 2016 Globosat. All rights reserved.
//

import Foundation

protocol OmdbSearcherDelegate : class {
	func omdbSearcher(didFindMovies movies: [MovieObject])
	func omdbSearcherDidFailedFindingMovies()
}

class OmdbSearcher {
	fileprivate let omdbApiUrl: String = "http://www.omdbapi.com/?s="
	
	fileprivate var session: URLSession = URLSession(configuration: .default)
	fileprivate var dataTask: URLSessionDataTask!
	
	weak var delegate: OmdbSearcherDelegate?
	
	init() {
		
	}
	
	deinit {
		self.session.invalidateAndCancel()
		
		self.dataTask = nil
	}
	
	func searchOmdb(byMovieTitle title: String) {
		if self.dataTask != nil {
			self.dataTask.cancel()
		}
		
		let searchUrl: String = "\(self.omdbApiUrl)\(title.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!)"
		let url: URL = URL(string: searchUrl)!
		self.dataTask = self.session.dataTask(with: url) { [weak self] (data: Data?, response: URLResponse?, err: Error?) in
			if err == nil {
				if data != nil {
					self?.parseData(data!)
				} else {
					self?.dispatchFailDelegate()
				}
			} else {
				if (err as! NSError).code != -999 {
					self?.dispatchFailDelegate()
				}
			}
		}
		self.dataTask.resume()
	}
	
	fileprivate func parseData(_ data: Data) {
		let jsonInfo: NSDictionary = try! JSONSerialization.jsonObject(with: data, options: .allowFragments) as! NSDictionary
		guard let search = jsonInfo["Search"] as? [NSDictionary] else { return }
		var movies: [MovieObject] = [MovieObject]()
		for item in search {
			movies.append(MovieObject(info: item))
		}
		self.dispatchMovies(movies)
		
		if self.dataTask != nil {
			self.dataTask = nil
		}
	}
	
	fileprivate func dispatchMovies(_ movies: [MovieObject]) {
		DispatchQueue.main.async { [weak self] in
			self?.delegate?.omdbSearcher(didFindMovies: movies)
		}
	}
	
	fileprivate func dispatchFailDelegate() {
		DispatchQueue.main.async { [weak self] in
			self?.delegate?.omdbSearcherDidFailedFindingMovies()
		}
	}
}
