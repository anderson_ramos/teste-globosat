//
//  MovieCell.swift
//  teste
//
//  Created by Anderson Lucas C. Ramos on 17/12/16.
//  Copyright © 2016 Globosat. All rights reserved.
//

import UIKit

class MovieCell : UICollectionViewCell {
	fileprivate var isGestureAdded: Bool = false
	
	@IBOutlet fileprivate(set) weak var thumbImageView: UIImageView!
	
	func addLongPressGestureIfNeeded(target: Any?, action: Selector?) {
		if !self.isGestureAdded {
			self.addGestureRecognizer(UILongPressGestureRecognizer(target: target, action: action))
			self.isGestureAdded = true
		}
	}
}
