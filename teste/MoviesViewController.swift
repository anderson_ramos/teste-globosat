//
//  MoviesViewController.swift
//  teste
//
//  Created by Anderson Lucas C. Ramos on 17/12/16.
//  Copyright © 2016 Globosat. All rights reserved.
//

import UIKit

class MoviesViewController : UICollectionViewController, UICollectionViewDelegateFlowLayout, UIViewControllerPreviewingDelegate, MovieDataDelegate {
	fileprivate var loadersForMovies: [MovieObject: MovieObjectDataLoader] = [MovieObject: MovieObjectDataLoader]()
	
	var movieList: [MovieObject]!
	
	fileprivate(set) var favoritesManager: FavoritesManager = FavoritesManager()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.reloadMovieList()
		
		self.setupActionButton()
		
		self.setupTraitSupport()
	}
	
	func reloadMovieList() {
		self.movieList = self.loadMovieList()
	}
	
	func loadMovieList() -> [MovieObject] {
		return MovieListParser().loadMovieList()
	}
	
	func setupActionButton() {
		self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .search,
		                                                         target: self,
		                                                         action: #selector(MoviesViewController.searchButton(sender:)))
	}
	
	func setupTraitSupport() {
		if self.isForceTouchAvailable() {
			if #available(iOS 9.0, *) {
				self.registerForPreviewing(with: self, sourceView: self.collectionView!)
			}
		}
	}
	
	func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
		guard let indexPath = self.collectionView?.indexPathForItem(at: self.collectionView!.convert(location, to: self.collectionView)) else { return nil }
		guard let cell = self.collectionView?.cellForItem(at: indexPath) else { return nil }
		let detail = self.detailViewController(forItem: indexPath.row)
		if #available(iOS 9.0, *) {
			previewingContext.sourceRect = cell.frame
		}
		return detail
	}
	
	func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
		self.show(viewControllerToCommit, sender: self)
	}
	
	func detailViewController(forItem index: Int) -> MovieViewerViewController {
		let detail = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "movieViewer") as! MovieViewerViewController
		let movieObject = self.movieListItem(index)
		detail.movieObject = movieObject
		detail.movieLoader = self.loadersForMovies[movieObject]
		return detail
	}
	
	func showSearchViewController() {
		let controller = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "omdbSearchMovies") as! OmdbSearchMoviesViewController
//		controller.movieList = self.movieList
		self.navigationController?.pushViewController(controller, animated: false)
	}
	
	@objc fileprivate func searchButton(sender: UIBarButtonItem?) {
		self.showSearchViewController()
	}
	
	func movieListCount() -> Int {
		return self.movieList.count
	}
	
	func movieListItem(_ index: Int) -> MovieObject {
		return self.movieList[index]
	}
	
	//// Collection View Controller Delegate Impl
	
	override func numberOfSections(in collectionView: UICollectionView) -> Int {
		return 1
	}
	
	override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return self.movieListCount()
	}
	
	override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell: MovieCell = collectionView.dequeueReusableCell(withReuseIdentifier: "MovieCell", for: indexPath) as! MovieCell
		
		let object: MovieObject = self.movieListItem(indexPath.row)
		var image: UIImage? = #imageLiteral(resourceName: "cover")
		if object.thumbImage != nil {
			 image = object.thumbImage
		}
		cell.thumbImageView.image = image
		self.loadMovieDataIfNeeded(object)
		
		if !self.isForceTouchAvailable() {
			cell.addLongPressGestureIfNeeded(target: self, action: #selector(MoviesViewController.longPressGesture(gesture:)))
		}
		
		return cell
	}
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		let width = (self.view.frame.width - 4 * 10) / 3 // granting 3 covers in portrait mode, obs: see layout size config for more understanding 
		return CGSize(width: width, height: width / 0.66) // 0.66 is the proportion
	}
	
	override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		self.navigationController?.pushViewController(self.detailViewController(forItem: indexPath.row), animated: true)
	}
	
	fileprivate func isForceTouchAvailable() -> Bool {
		if #available(iOS 9.0, *) {
			return self.traitCollection.forceTouchCapability == .available
		}
		
		return false
	}
	
	@objc fileprivate func longPressGesture(gesture: UILongPressGestureRecognizer?) {
		let index: Int = self.collectionView!.indexPath(for: gesture!.view as! MovieCell)!.row
		let movie: MovieObject = self.movieListItem(index)
		
		let alert: UIAlertController = UIAlertController(title: "O que fazer?", message: nil, preferredStyle: .actionSheet)
		if self.favoritesManager.isMovieFavorited(movie) {
			alert.addAction(UIAlertAction(title: "Remover dos favoritos", style: .default, handler: { [weak self] (action: UIAlertAction) in
				self?.favoritesManager.remove(movie)
			}))
		} else {
			alert.addAction(UIAlertAction(title: "Adicionar aos favoritos", style: .default, handler: { [weak self] (action: UIAlertAction) in
				self?.favoritesManager.add(movie)
			}))
		}
		if UI_USER_INTERFACE_IDIOM() == .pad {
			let controller = alert.popoverPresentationController
			controller?.sourceView = gesture?.view
			controller?.sourceRect = gesture!.view!.bounds
		} else {
			alert.addAction(UIAlertAction(title: "Cancelar", style: .destructive, handler: { (action: UIAlertAction) in
				
			}))
		}
		self.present(alert, animated: true, completion: nil)
	}
	
	fileprivate func loadMovieDataIfNeeded(_ object: MovieObject) {
		if object.thumbImage == nil {
			let contains = self.loadersForMovies.contains { (pair: (key: Any, value: Any)) -> Bool in
				guard let key = pair.key as? MovieObject else { return false }
				return key.id == object.id
			}
			
			if !contains {
				let loader: MovieObjectDataLoader = MovieObjectDataLoader()
				self.loadersForMovies[object] = loader
				loader.delegate = self
				loader.loadDetailedData(forObject: object)
			}
		}
	}
	
	////// Movie Data Delegate Impl
	
	func movieDidLoadData(_ movie: MovieObject) {
		let index = self.movieList.index { (object: MovieObject) -> Bool in
			return object.id == movie.id
		}
		
		if index != nil {
			let cell: MovieCell? = self.collectionView?.cellForItem(at: IndexPath(row: index!, section: 0)) as? MovieCell
			if cell != nil {
				cell?.thumbImageView.image = movie.thumbImage
				cell?.setNeedsDisplay()
			}
		}
		
		self.loadersForMovies.removeValue(forKey: movie)
		self.collectionView?.reloadData()
	}
	
	func movieDidFailedToLoadData(_ movie: MovieObject) {
		
	}
}
