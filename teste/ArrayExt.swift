//
//  ArrayExt.swift
//  teste
//
//  Created by Anderson Lucas C. Ramos on 17/12/16.
//  Copyright © 2016 Globosat. All rights reserved.
//

extension Array where Element: Equatable {	
	// Remove first collection element that is equal to the given `object`:
	mutating func remove(object: Element) {
		if let index = index(of: object) {
			remove(at: index)
		}
	}
}
