//
//  MovieObject.swift
//  teste
//
//  Created by Anderson Lucas C. Ramos on 16/12/16.
//  Copyright © 2016 Globosat. All rights reserved.
//

import UIKit

protocol MovieDataDelegate : class {
	func movieDidLoadData(_ movie: MovieObject)
	func movieDidFailedToLoadData(_ movie: MovieObject)
}

class MovieObject : BasicJSONObject {
	fileprivate(set) var isDetailedDataLoaded: Bool = false
	
	fileprivate var thumb: String? {
		return self.get(key: "Poster")
	}
	
	var id: String {
		return self.get(key: "imdbID")!
	}
	
	var title: String? {
		return self.get(key: "Title")
	}
	
	var duration: String? {
		return self.get(key: "Runtime")
	}
	
	var awards: String? {
		return self.get(key: "Awards")
	}
	
	var releaseDate: String? {
		return self.get(key: "Released")
	}
	
	var rating: String? {
		return self.get(key: "imdbRating")
	}
	
	var genre: String? {
		return self.get(key: "Genre")
	}
	
	var director: String? {
		return self.get(key: "Director")
	}
	
	var plot: String? {
		return self.get(key: "Plot")
	}
	
	fileprivate(set) var thumbImage: UIImage?
	
	fileprivate func loadDetailedInfo(_ info: NSDictionary) {
		self.isDetailedDataLoaded = true
		
		self.addComplementalInfo(info)
	}
}

class MovieObjectDataLoader {
	fileprivate let omdbApi: String = "http://www.omdbapi.com/?i="
	
	fileprivate let session: URLSession = URLSession(configuration: .default)
	fileprivate var detailTask: URLSessionDataTask!
	fileprivate var thumbTask: URLSessionDataTask!
	
	weak var delegate: MovieDataDelegate?
	
	deinit {
		self.session.invalidateAndCancel()
		
		self.detailTask = nil
		self.thumbTask = nil
	}
	
	func loadDetailedData(forObject object: MovieObject) {
		if object.isDetailedDataLoaded {
			self.loadThumbImage(forObject: object)
			
			return
		}
		
		let completeUrl: String = "\(self.omdbApi)\(object.id)"
		
		self.detailTask = self.session.dataTask(with: URL(string: completeUrl)!) { [weak self] (data: Data?, response: URLResponse?, err: Error?) in
			if err == nil {
				if data != nil {
					self?.parseData(data!, object)
					self?.loadThumbImage(forObject: object)
				} else {
					self?.dispatchDelegate(forObject: object, success: false)
				}
			} else {
				if (err as! NSError).code != -999 {
					self?.dispatchDelegate(forObject: object, success: false)
				}
			}
		}
		
		self.detailTask.resume()
	}
	
	func loadThumbImage(forObject object: MovieObject) {
		if object.thumbImage != nil || object.thumb == nil {
			return
		}
		
		self.thumbTask = self.session.dataTask(with: URL(string: object.thumb!)!) { [weak self] (data: Data?, response: URLResponse?, err: Error?) in
			if err == nil {
				if data != nil {
					let image = UIImage(data: data!)
					object.thumbImage = image
					
					self?.dispatchDelegate(forObject: object, success: true)
				} else {
					self?.dispatchDelegate(forObject: object, success: false)
				}
			} else {
				if (err as! NSError).code != -999 && (err as! NSError).code != -1002 {
					self?.dispatchDelegate(forObject: object, success: false)
				}
			}
		}
		
		self.thumbTask.resume()
	}
	
	func cancelPendingRequest() {
		if self.detailTask != nil {
			self.detailTask.cancel()
			self.detailTask = nil
		}
		
		if self.thumbTask != nil {
			self.thumbTask.cancel()
			self.thumbTask = nil
		}
	}
	
	fileprivate func parseData(_ data: Data, _ object: MovieObject) {
		let jsonInfo: NSDictionary = try! JSONSerialization.jsonObject(with: data, options: .allowFragments) as! NSDictionary
		object.loadDetailedInfo(jsonInfo)
	}
	
	fileprivate func dispatchDelegate(forObject object: MovieObject, success: Bool) {
		DispatchQueue.main.async { [weak self] in
			if success {
				self?.delegate?.movieDidLoadData(object)
			} else {
				self?.delegate?.movieDidFailedToLoadData(object)
			}
		}
	}
}
