//
//  GEObject.swift
//  Seleção Globo.com
//
//  Created by Anderson Lucas C. Ramos on 04/05/15.
//  Copyright (c) 2015 Glogo.com. All rights reserved.
//

import Foundation

class BasicJSONObject : NSObject {
	fileprivate var intern: NSMutableDictionary = NSMutableDictionary()
	
	init(info: NSDictionary) {
		super.init()
		self.addComplementalInfo(info)
	}
	
	internal func addComplementalInfo(_ info: NSDictionary) {
		self.intern.addEntries(from: info as! [AnyHashable: Any])
	}
	
	internal func get<T>(key: String) -> T? {
		return self.intern[key] as? T
	}
	
	internal func set<T>(key: String, value: T) {
		self.intern[key] = value
	}
}
