//
//  Session.swift
//  teste
//
//  Created by Anderson Lucas C. Ramos on 18/12/16.
//  Copyright © 2016 Globosat. All rights reserved.
//

import Foundation

protocol SessionDelegate : class {
	func session(didAuthenticateWithSuccess success: Bool)
	func session(connectionError err: Error?)
	func sessionCannotProceedWithEmail()
	func sessionCannotProceedWithPassword()
}

class Session {
	fileprivate let session: URLSession = URLSession(configuration: .default)
	fileprivate var dataTask: URLSessionDataTask!
	
	weak var delegate: SessionDelegate?
	
	var isAuthenticated: Bool {
		return UserDefaults.standard.string(forKey: "sessionKey") != nil
	}
	
	var authToken: String? {
		return UserDefaults.standard.string(forKey: "sessionKey")
	}
	
	deinit {
		self.session.invalidateAndCancel()
	}
	
	func login(withEmail email: String, andPassword password: String) {
		if !self.validate(email: email, andPassword: password) {
			return
		}
		
		if self.dataTask != nil {
			self.dataTask.cancel()
		}
		
		let url: URL = URL(string: "http://google.com")! // it's just to simulate a request
		self.dataTask = self.session.dataTask(with: url, completionHandler: { [weak self] (data: Data?, response: URLResponse?, err: Error?) in
			if err == nil {
				if data != nil {
					self?.saveSession(uuidSession: self!.getAuthToken())
					if email == "teste@globosat.com" && password == "123456" {
						self?.dispatchSuccess()
					} else {
						self?.dispatchError(err: nil)
					}
				} else {
					self?.dispatchError(err: nil)
				}
			} else {
				if (err as! NSError).code != -999 { // means that the dataTask was canceled
					self?.dispatchError(err: err)
				}
			}
		})
		self.dataTask.resume()
	}
	
	func cancelPending() {
		if self.dataTask != nil {
			self.dataTask.cancel()
		}
	}
	
	fileprivate func validate(email: String, andPassword password: String) -> Bool {
		if email.isEmpty || !email.contains("@") || !email.contains(".") {
			DispatchQueue.main.async { [weak self] in
				self?.delegate?.sessionCannotProceedWithEmail()
			}
			return false
		}
		
		if password.isEmpty || password.lengthOfBytes(using: .utf8) < 6 {
			DispatchQueue.main.async { [weak self] in
				self?.delegate?.sessionCannotProceedWithPassword()
			}
			return false
		}
		
		return true
	}
	
	fileprivate func getAuthToken() -> String {
		return UUID().uuidString
	}
	
	fileprivate func saveSession(uuidSession: String) {
		UserDefaults.standard.set(uuidSession, forKey: "sessionKey")
	}
	
	fileprivate func dispatchSuccess() {
		DispatchQueue.main.async { [weak self] in
			self?.delegate?.session(didAuthenticateWithSuccess: true)
		}
	}
	
	fileprivate func dispatchError(err: Error?) {
		DispatchQueue.main.async { [weak self] in
			if err != nil {
				self?.delegate?.session(connectionError: err)
			} else {
				self?.delegate?.session(didAuthenticateWithSuccess: false)
			}
		}
	}
}
